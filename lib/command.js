"use strict";

const { print } = require("../helper");
const { add, remove, get, list, clear } = require("./dictionary");

const ADD = "add";
const REMOVE = "remove";
const GET = "get";
const LIST = "list";
const CLEAR = "clear";
const EMPTY = "";

exports = module.exports = handleCommands;

function handleCommands(args) {
  let validKey = true,
    validVal = true;

  const cmd = args[0] ? args[0].toLowerCase() : "",
    key = args[1] ? args[1] : (validKey = false),
    val = args[2] ? args[2] : (validVal = false);

  switch (cmd) {
    case ADD:
      validKey && validVal
        ? add(key, val)
        : print("Invalid arguments", "Please provide a key/value pair");
      break;

    case REMOVE:
      validKey
        ? remove(key)
        : print("Invalid arguments", "Please provide a key");
      break;

    case GET:
      validKey ? get(key) : print("Invalid arguments", "Please provide a key");
      break;

    case LIST:
      list();
      break;

    case CLEAR:
      clear();
      break;

    case EMPTY:
      print(
        "Empty arguments",
        `Kindly choose one from the following (${ADD} - ${REMOVE} - ${GET} - ${LIST} - ${CLEAR})`
      );
      break;

    default:
      print(
        "UNSUPPORTED COMMAND",
        `Kindly choose one from the following (${ADD} - ${REMOVE} - ${GET} - ${LIST} - ${CLEAR})`
      );
      break;
  }
}
