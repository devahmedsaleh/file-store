"use strict";

const MemCache = (function() {
  let _cache = new Map();

  const set = (key, val) => _cache.set(key, val);

  const remove = key => _cache.delete(key);

  const get = key => _cache.get(key);

  const list = () => _cache;

  const clear = () => _cache.clear();

  return {
    Add: set,
    Remove: remove,
    Get: get,
    List: list,
    Clear: clear
  };
})();

exports = module.exports = MemCache;
